# compile coffee scripts in something dir.

This Cakefile compiles all of coffee scripts in target directory (include sub directories).

## install

```
npm install
```

## useage

### help

```
cake
```

### compile

default

```
cake compile
```

or set options

```
cake -s ./source_directory -o ../js/compiled.js compile
```

## ./src

This is sample application of "Backbone.js".