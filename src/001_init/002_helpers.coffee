# URLと渡したパスが一致すればtrue、一致しなければfalseを返す
# @args string|RegExp path
# @return boolean
App.helpers.match_url = (path) ->
  if typeof(path).toLowerCase() is 'string'
    if path is '/'
      path = new RegExp('^/$')
    else
      path = new RegExp('^/' + path.replace(/(^\/|\/$)/g, '') + '/?$')
  location.pathname.match(path) isnt null

# console.log の alias
App.helpers.log = ->
  console.log(arguments) if console?.log? and typeof(console.log).toLowerCase() is 'function'
log = App.helpers.log # set alias